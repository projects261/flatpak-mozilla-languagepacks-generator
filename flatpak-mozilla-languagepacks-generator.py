#!/usr/bin/env python3

import argparse
import aiohttp
import asyncio
import json
import logging
import re
from packaging.version import Version
from typing import Dict, List, Optional

import aiohttp

GENERATOR_CHECK_ROOT = {
    "thunderbird-nightly": "https://download-installer.cdn.mozilla.net/pub/thunderbird/nightly/latest-comm-central-l10n/",
    "firefox-nightly": "https://download-installer.cdn.mozilla.net/pub/firefox/nightly/latest-mozilla-central-l10n/",
}
GENERATOR_LANGUAGEPACK_NAME_PATTERNS = {
    "firefox-nightly": r"firefox-([a\d\.-]+)\.([a-zA-Z-]+)\.langpack\.xpi",
    "thunderbird-nightly": r"thunderbird-([a\d\.-]+)\.([a-zA-Z-]+)\.langpack\.xpi",
}
GENERATOR_CHECKSUMS_NAME_PATTERNS = {
    "firefox-nightly": r"firefox-([a\d\.-]+)\.([a-zA-Z-]+)\.linux-x86_64\.checksums",
    "thunderbird-nightly": r"thunderbird-([a\d\.-]+)\.([a-zA-Z-]+)\.linux-x86_64\.checksums",
}
GENERATOR_PRODUCT_NAMES = {
    "firefox-nightly": "firefox",
    "thunderbird-nightly": "thunderbird",
}
GENERATOR_SHA512_PATTERN = r"(\w{128})\ssha512\s\d+\s[a-zA-Z0-9.-]+\.langpack\.xpi"
log = logging.getLogger(__name__)


class MozillaLanguagepacksGenerator:
    session: aiohttp.ClientSession
    product_type: str

    def __init__(self, product_type: str):
        self.product_type = product_type

    def get_check_url(self):
        return f"{GENERATOR_CHECK_ROOT[self.product_type]}"

    def get_languagepack_root(self):
        return f"{GENERATOR_CHECK_ROOT[self.product_type]}linux-x86_64/xpi/"

    def get_languagepack_name(self, version: str, language: str) -> str:
        return f"{GENERATOR_PRODUCT_NAMES[self.product_type]}-{version}.{language}.langpack.xpi"

    def get_checksum_name(self, version: str, language: str) -> str:
        return f"{GENERATOR_PRODUCT_NAMES[self.product_type]}-{version}.{language}.linux-x86_64.checksums"

    def get_checksum_url(self, version: str, language: str) -> str:
        return f"{GENERATOR_CHECK_ROOT[self.product_type]}{self.get_checksum_name(version,language)}"

    def get_latest_version(self, html: str) -> Optional[str]:
        sort_key = lambda m: Version(m[0])
        pattern = re.compile(GENERATOR_CHECKSUMS_NAME_PATTERNS[self.product_type])
        match = pattern.findall(html)
        if not match:
            log.warning("%s did not match", pattern.pattern)
            return None
        if sort_key is None or len(match) == 1:
            result = match[0][0]
        else:
            log.debug("%s matched multiple times, selected latest", pattern.pattern)
            result = max(match, key=sort_key)[0]
        return result

    def get_languages(self, html: str) -> Optional[List[str]]:
        pattern = re.compile(GENERATOR_CHECKSUMS_NAME_PATTERNS[self.product_type])
        match = pattern.findall(html)
        if not match:
            log.warning("%s did not match", pattern.pattern)
            return None
        else:
            languages = list(set([i[1] for i in match]))
            return languages

    def get_languagepack_url(self, version: str, lang: str) -> str:
        return self.get_languagepack_root() + self.get_languagepack_name(version, lang)

    async def get_languagepack_sha512(self, version: str, lang: str) -> Optional[str]:
        url = self.get_checksum_url(version, lang)
        async with self.session.get(url) as response:
            pattern = re.compile(GENERATOR_SHA512_PATTERN)
            match = pattern.findall(await response.text())
            if match:
                sha512 = match[0]
                return sha512

    async def get_source_item(
        self, version: str, lang: str
    ) -> Dict[str, Optional[str]]:
        url = self.get_languagepack_url(version, lang)
        sha512 = await self.get_languagepack_sha512(version, lang)
        return {"type": "file", "url": url, "sha512": sha512, "dest": "langpacks"}

    async def generate(self):
        async with aiohttp.ClientSession() as session:
            self.session = session
            async with session.get(self.get_check_url()) as response:
                html = await response.text()
                latest_version = self.get_latest_version(html)
                if latest_version is None:
                    log.warning("No latest version found for %s", self.product_type)
                    return None
                languages = self.get_languages(html)
                if 'en-US' in languages:
                    languages.remove('en-US')
                else:
                    languages = languages
                sources = await asyncio.gather(
                    *[
                        self.get_source_item(latest_version, lang)
                        for lang in languages
                    ]
                )
                with open("generated-sources.json", mode="w") as f:
                    f.write(json.dumps(sources, indent=2))

parser = argparse.ArgumentParser()
parser.add_argument(
    "--product-type",
    help="Product type, e.g. thunderbird-nightly or firefox-nightly",
    default="firefox-nightly",
)
args = parser.parse_args()
product_type = args.product_type.lower()


async def main():
    generator = MozillaLanguagepacksGenerator(product_type)
    await generator.generate()

asyncio.run(main())
